package com.example.calculator205150407111035

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity2 : AppCompatActivity() {
    lateinit var txtInput: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        txtInput = findViewById(R.id.textView3)
        var result = intent.getStringExtra("result")
        txtInput.text = result
    }
}